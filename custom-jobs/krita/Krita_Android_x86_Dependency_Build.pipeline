// Request a node to be allocated to us
node( "AndroidSDK" ) {
// We want Timestamps on everything
timestamps {
	// We want to catch any errors that occur to allow us to send out notifications (ie. emails) if needed
	catchError {

		// First Thing: Checkout Sources
		stage('Checkout Sources') {
			// Make sure we have a clean slate to begin with
			deleteDir()

			// Krita Code
			checkout changelog: true, poll: true, scm: [
				$class: 'GitSCM',
				branches: [[name: 'master']],
				extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: 'krita/']],
				userRemoteConfigs: [[url: 'https://invent.kde.org/graphics/krita.git']]
			]

		}

		// Make sure the Android SDK is setup properly
		stage('Setting up SDK') {
			// For this we need to ensure that the Android 24 or later SDK is installed, otherwise the APK generation will fail
			sh """
				sdkmanager "platforms;android-28"
				sdkmanager "ndk;18.1.5063045"
			"""
		}

		// Now we can build the Dependencies for Krita's build
		stage('Building Dependencies') {
			// This is relatively straight forward
			// For this we do need to unset QT_ANDROID though, to ensure it builds it's own patched version rather than using the normal one we rely on elsewhere
			// Then we invoke them!
			sh """
				unset QT_ANDROID
				unset QMAKESPEC
				
				export ANDROID_ABI=x86
				export ANDROID_API_LEVEL=28
				export ANDROID_NDK=$ANDROID_SDK_ROOT/ndk/18.1.5063045/
				export ANDROID_NDK_ROOT=$ANDROID_NDK
				export CMAKE_ANDROID_NDK=$ANDROID_NDK
				
				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=boost
				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=qt
				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=3rdparty
				krita/packaging/android/androidbuild.sh --src=$WORKSPACE/krita/ --build-type=Release --build-root=$WORKSPACE/build/ -p=kf5
			"""
		}

		// Now we capture them for use
		stage('Capturing Dependencies') {
			// First we tar all of the dependencies up...
			sh """
				tar -cf $WORKSPACE/krita-android-deps.tar build/i/ build/kf5/kde/install/
			"""

			// Then we ask Jenkins to capture the tar file as an artifact
			archiveArtifacts artifacts: 'krita-android-deps.tar', onlyIfSuccessful: true
		}
	}
}
}
