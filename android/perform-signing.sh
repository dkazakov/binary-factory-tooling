#!/bin/bash
set -e

# Make sure local shell scripts are in PATH
export PATH=$HOME/bin/:$PATH

# Load our parameters
application=$1

# Load in the default Android signing configuration
source ~/keys/android-signing-config

# Check to see if we have an application specific signing configuration to use
if [ -f ~/keys/${application}-signing-config ]; then
	source ~/keys/$application-signing-config
fi

for apk in `echo *.apk`; do
	zipalign -f 4 $apk aligned-$apk || true
	zipalign -f 4 aligned-$apk signed-$apk
	rm aligned-$apk

	apksigner sign $sign_parameters signed-$apk

	rm $apk
	mv signed-$apk $apk
done
